
# 4- Bien tester
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
Objectifs :
 * Etre efficace dans l'écriture des tests
 * Rentabiliser et capitaliser

----

## De bons outils : EclEmma
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Vérifier l'installation / installer EclEmma
* Quel est le taux de couverture des tests ?
* Quelle classe / méthode n'est pas suffisamment testée ?

----

## De bons outils : Infinitest
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Vérifier l'installation / installer Infinitest
* Modifier le code d'un test, sauvegarder le fichier, apprécier
* Modifier le code d'une méthode, sauvegarder le fichier, apprécier

----

## De bons outils : MoreUnit
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Vérifier l'installation / installer MoreUnit
* Faire click droit sur du code -> MoreUnit et consulter la liste des raccourcis
* Tester Ctrl + J / Ctrl + U / Ctrl + R

----

## Et mon projet dans tout ça ?
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Consulter les statistiques du projet sur la PIC
* Récupérer le code
* Vérifier les dépendances utilisées et leurs versions (JUnit ? DBUnit ? Base de données embarquées ? Mockito ?)
* Y a-t-il une Javadoc ? Est elle bien écrite ?
* Exécuter les tests
* Ecrire un premier test unitaire
