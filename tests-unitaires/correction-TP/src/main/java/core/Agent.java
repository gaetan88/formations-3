package core;

import services.AgentService;

public class Agent {

	private String idep;

	public Agent() {
		
	}
	
	/**
	 * Crée un nouvel agent avec l'idep idep
	 * @param idep L'idep de l'agent à créer
	 * @throws IllegalArgumentException Si l'idep de l'agent est null ou ne fait pas 6 caractères
	 */
	public Agent(String idep) throws IllegalArgumentException {
		AgentService agentService = new AgentService();
		if (!agentService.isIdepValide(idep)) {
			throw new IllegalArgumentException();
		}
		setIdep(idep);
	}
	
	public String getIdep() {
		return idep;
	}

	public void setIdep(String idep) {
		this.idep = idep;
	}
}
