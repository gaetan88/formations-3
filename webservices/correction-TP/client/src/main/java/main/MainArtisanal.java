package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class MainArtisanal {

	public static void main(String[] args) throws Exception {
		InputStream stream = new URL(URLS.HELLO).openConnection().getInputStream();
		readStream(stream);
	}
	
	private static void readStream(InputStream stream) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		String ligne;
		while ((ligne = reader.readLine()) != null) {
			System.out.println(ligne);
		}
	}

}
