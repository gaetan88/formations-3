#Formation Java avanc�
##Public cible
Cette formation s'adresse � des d�veloppeurs ayant d�j� une exp�rience en d�veloppement Java et souhaitant d�couvrir de nouvelles fonctionnalit�s du langage, prendre du recul et comprendre comment certains choses fonctionnent, gagner en aisance, en productivit� et en qualit� de code.
##Objectifs
L'objectif de cette formation est d'amener les d�veloppeurs � aller plus loin en Java en leur faisant (re)d�couvrir des possibilit�s du langage souvent sous-exploit�es soit par m�connaissance soit par crainte de leur complexit�.      
Les b�n�fices recherch�s sont multiples :  

- Faire prendre du recul aux d�veloppeurs  
- Renforcer la confiance en eux des d�veloppeurs  
- D�mystifier le code �crit par d'autres  ("Hibernate c'est de la magie, je serais incapable de faire pareil")  
- Am�liorer la productivit� des d�veloppeurs  
- Sensibiliser � l'�criture de code g�n�rique, modulaire, flexible et maintenable  

##Organisation
La formation est pr�vue sur 2 jours.  
Les notions th�oriques sont r�duites � l'essentiel pour laisser plus de place � la pratique.  
La formation s'articule autour d'un TP fil rouge consistant � �crire un parser (lecteur) CSV le plus g�n�rique, flexible et modulaire possible.  
Le principe de chaque notion est expliqu� rapidement et directement mis en application dans le TP fil rouge.

##Notions abord�es  
- Types param�tr�s  
- Introspection / r�flexivit�  
- Annotations  
- Multithreading  
- M�thodes de la classe Object  
- Syntaxe Java 8  